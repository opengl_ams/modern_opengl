#include "Mesh.h"

Mesh::Mesh(): VAO(0), VBO(0), IBO(0), indexCount(0) {
}

void Mesh::CreateMesh(GLfloat *vertices, unsigned int *indices, unsigned int numOfVertices, unsigned int numOfIndices) {

	indexCount = numOfIndices;

	glGenVertexArrays(1, &VAO);	// Generate VAO ID
	glBindVertexArray(VAO);		// Bind the VAO to the ID

	glGenBuffers(1, &IBO);	// Generate IBO ID
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);	// Bint the IBO to the id
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices[0]) * numOfIndices, indices, GL_STATIC_DRAW);	// Attach indices to IBO

	glGenBuffers(1, &VBO);	//	Generate VBO ID
	glBindBuffer(GL_ARRAY_BUFFER, VBO);	// Bind the VBO to the ID (work on the chosen VBO attached to the chosen VAO)
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices[0]) * numOfVertices, vertices, GL_STATIC_DRAW);	// Attach vertex to VBO

	// index 0, components 3, type, normalized?, byte offset to next generic vertex attr 5, offset of first component 0
	// 0, 0, 0, 0, 0, index 0 - 2, then jumps to 5 and reads the next 3
	// 0, 0, 0, 0, 0
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vertices[0]) * 8, 0);	// Define attribute pointer formatting
	glEnableVertexAttribArray(0);	// Enable attribute pointer
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(vertices[0]) * 8, reinterpret_cast<void*>(sizeof(vertices[0]) * 3));	// Define attribute pointer formatting
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(vertices[0]) * 8, reinterpret_cast<void*>(sizeof(vertices[0]) * 5));	// Define attribute pointer formatting
	glEnableVertexAttribArray(2);

	glBindBuffer(GL_ARRAY_BUFFER, 0);	// Unbind VBO

	glBindVertexArray(0);	// Unbind VAO, ready for next object

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);	// Unbind IBO
}


void Mesh::RenderMesh() {

	glBindVertexArray(VAO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
	glDrawElements(GL_TRIANGLES, indexCount, GL_UNSIGNED_INT, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
}

void Mesh::ClearMesh() {

	if (IBO != 0) {
		glDeleteBuffers(1, &IBO);
		IBO = 0;
	}

	if (VBO != 0) {
		glDeleteBuffers(1, &VBO);
		VBO = 0;
	}

	if (VAO != 0) {
		glDeleteVertexArrays(1, &VAO);
		VAO = 0;
	}

	indexCount = 0;
}

Mesh::~Mesh() {
	ClearMesh();
}


#include "OpenGLWindow.h"


OpenGLWindow::OpenGLWindow():
mainWindow(nullptr), width(800), height(600), bufferWidth(0), bufferHeight(0), keys{0}, lastX{0}, lastY{0}, xChange{0}, yChange{0}, mouseFirstMoved{true} {}

OpenGLWindow::OpenGLWindow(GLint windowWidth, GLint windowHeight)
	: mainWindow(nullptr), width(windowWidth), height(windowHeight), bufferWidth(0), bufferHeight(0), keys{0}, lastX{0}, lastY{0}, xChange{0}, yChange{0}, mouseFirstMoved{true} {}

int OpenGLWindow::Initialize() {

	// Init GLFW
	if (!glfwInit()){
		std::cout << "GLFW initialization failed!" << std::endl;
		glfwTerminate();
		return 1;
	}

	// Setup GLFW window properties
	// OpenGL version
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	// Core profile = No Backwards Compatibility
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	// Allow forward compatibility
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);

	mainWindow = glfwCreateWindow(width, height, "Test Window", NULL, NULL);
	if (!mainWindow) {
		std::cout << "GLFW window creation failed!" << std::endl;
		glfwTerminate();
		return 1;
	}

	// Get Buffer size information
	glfwGetFramebufferSize(mainWindow, &bufferWidth, &bufferHeight);

	// Set the context for GLEW to use
	glfwMakeContextCurrent(mainWindow);

	// handle key + mouse input
	createCallBacks();
	glfwSetInputMode(mainWindow, GLFW_CURSOR, GLFW_CURSOR_DISABLED); // turns off cursor

	// Allow modern extension features
	glewExperimental = GL_TRUE;

	GLenum error = glewInit();
	if(error != GLEW_OK) {
		std::cout << "GLEW initialization failed! Error: " <<  glewGetErrorString(error) << std::endl;
		glfwDestroyWindow(mainWindow);
		glfwTerminate();
		return 1;
	}

	glEnable(GL_DEPTH_TEST);

	// Create viewport
	glViewport(0, 0, bufferWidth, bufferHeight);

	glfwSetWindowUserPointer(mainWindow, this);

	return 0;
}

void OpenGLWindow::createCallBacks() {

	glfwSetKeyCallback(mainWindow, handleKeys);
	glfwSetCursorPosCallback(mainWindow, handleMouse);
}

GLfloat OpenGLWindow::getXChange() {

	GLfloat theChange = xChange;
	xChange = 0.0f;
	return theChange;
}


GLfloat OpenGLWindow::getYChange() {

	GLfloat theChange = yChange;
	yChange = 0.0f;
	return theChange;
}

void OpenGLWindow::handleKeys(GLFWwindow *window, int key, int code, int action, int mode) {

	OpenGLWindow *theWindow = static_cast<OpenGLWindow*>(glfwGetWindowUserPointer(window));

	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS) {
		glfwSetWindowShouldClose(window, GL_TRUE);
	}

	if (key >= 0 && key < 1024) {

		if (action == GLFW_PRESS) {
			theWindow->keys[key] = true;
		} else if(action == GLFW_RELEASE) {
			theWindow->keys[key] = false;
		}
	}
}

void OpenGLWindow::handleMouse(GLFWwindow *window, double xPos, double yPos) {

	OpenGLWindow *theWindow = static_cast<OpenGLWindow*>(glfwGetWindowUserPointer(window));

	if (theWindow->mouseFirstMoved) {
		theWindow->lastX = xPos;
		theWindow->lastY = yPos;
		theWindow->mouseFirstMoved = false;
	}

	theWindow->xChange = xPos - theWindow->lastX;
	theWindow->yChange = theWindow->lastY - yPos;

	theWindow->lastX = xPos;
	theWindow->lastY = yPos;
}

OpenGLWindow::~OpenGLWindow() {

	glfwDestroyWindow(mainWindow);
	glfwTerminate();
}


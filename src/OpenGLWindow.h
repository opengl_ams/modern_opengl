#ifndef OPENGLWINDOW_H_
#define OPENGLWINDOW_H_

#include <iostream>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

class OpenGLWindow {
public:
	OpenGLWindow();
	OpenGLWindow(GLint windowWidth, GLint windowHeight);

	int Initialize();

	GLfloat getBufferWidth() { return bufferWidth; }
	GLfloat getBufferHeight() { return bufferHeight; }

	bool getShouldClose() { return glfwWindowShouldClose(mainWindow); }

	bool* getKeys() { return keys; }
	GLfloat getXChange();
	GLfloat getYChange();

	void swapBuffers() { return glfwSwapBuffers(mainWindow); }

	virtual ~OpenGLWindow();

private:
	GLFWwindow *mainWindow;

	GLint width, height;
	GLint bufferWidth, bufferHeight;

	bool keys[1024];

	GLfloat lastX;
	GLfloat lastY;
	GLfloat xChange;
	GLfloat yChange;
	bool mouseFirstMoved;

	void createCallBacks();
	static void handleKeys(GLFWwindow *window, int key, int code, int action, int mode);
	static void handleMouse(GLFWwindow *window, double xPos, double yPos);
};

#endif /* OPENGLWINDOW_H_ */

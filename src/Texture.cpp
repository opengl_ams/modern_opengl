#include <iostream>

#include "Texture.h"

Texture::Texture(): textureID(0), width(0), height(0), bitDepth(0), fileLocation(nullptr) {
}

Texture::Texture(const char *fileLoc): textureID(0), width(0), height(0), bitDepth(0), fileLocation(fileLoc) {}

void Texture::UseTexture() {

	glActiveTexture(GL_TEXTURE0); // set which texture unit to use, allows to bind texture
	glBindTexture(GL_TEXTURE_2D, textureID); // bind the texture, to the texture unit;
}

bool Texture::LoadTextureA() {

	// load image into char array
	unsigned char *textData = stbi_load(fileLocation, &width, &height, &bitDepth, 0);
	if (!textData) {
		std::cout << "Failed to find: " << fileLocation << std::endl;
		return false;
	}

	glGenTextures(1, &textureID); // generate texture and apply id to it
	glBindTexture(GL_TEXTURE_2D, textureID);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); // x-axis, repeat texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); // y-axis, repeat texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); // texture further away, linear filter
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // texture close, linear filter

	// set up image data from textData to the bound texture
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, textData);
	glGenerateMipmap(GL_TEXTURE_2D); // generate mipmap automatically to whatever is currently in GL_TEXTURE_2D

	glBindTexture(GL_TEXTURE_2D, 0); // unbind texrue

	stbi_image_free(textData);	// textdata was copied to the texture and can be freed for further reuse

	return true;
}

bool Texture::LoadTexture() {

	// load image into char array
	unsigned char *textData = stbi_load(fileLocation, &width, &height, &bitDepth, 0);
	if (!textData) {
		std::cout << "Failed to find: " << fileLocation << std::endl;
		return false;
	}

	glGenTextures(1, &textureID); // generate texture and apply id to it
	glBindTexture(GL_TEXTURE_2D, textureID);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); // x-axis, repeat texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); // y-axis, repeat texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); // texture further away, linear filter
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); // texture close, linear filter

	// set up image data from textData to the bound texture
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, textData);
	glGenerateMipmap(GL_TEXTURE_2D); // generate mipmap automatically to whatever is currently in GL_TEXTURE_2D

	glBindTexture(GL_TEXTURE_2D, 0); // unbind texrue

	stbi_image_free(textData);	// textdata was copied to the texture and can be freed for further reuse

	return true;
}

void Texture::ClearTexture() {

	glDeleteTextures(1, &textureID);
	textureID = 0;
	width = 0;
	height = 0;
	bitDepth = 0;
	fileLocation = "";
}

Texture::~Texture() {

	ClearTexture();
}


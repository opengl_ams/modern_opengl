#include "Shader.h"

Shader::Shader(): pointLightCount{0}, spotLightCount{0}, shaderID(0), uniformProjection(0),
					uniformModel(0), uniformView{0}, uniformEyePosition{0},
					uniformSpecularIntensity{0}, uniformShininess{0}, uniformPointLightCount{0},
					uniformSpotLightCount{0} {}

void Shader::CreateFromString(const char* vertexCode, const char* fragmentCode) {

	CompileShader(vertexCode, fragmentCode);
}

void Shader::CreateFromFiles(const char* vertexLocation, const char* fragmentLocation) {

	std::string vertexString = ReadFile(vertexLocation);
	std::string fragmentString = ReadFile(fragmentLocation);
	const char* vertexCode = vertexString.c_str();
	const char* fragmentCode = fragmentString.c_str();

	CompileShader(vertexCode, fragmentCode);
}

std::string Shader::ReadFile(const char* fileLocation) {

	std::string content;
	std::ifstream fileStream(fileLocation, std::ios::in);

	if (!fileStream.is_open()) {
		std::cout << "Failed to read! File doesn't exist: " << fileLocation << std::endl;
	}

	std::string line{""};
	while (!fileStream.eof()) {

		std::getline(fileStream, line);
		content.append(line + "\n");
	}

	fileStream.close();
	return content;
}

void Shader::CompileShader(const char* vertexCode, const char* fragmentCode) {

	shaderID = glCreateProgram(); // create shader program and put it into global id

	if (!shaderID) {
		std::cout << "Error creating shader program" << std::endl;
		return;
	}

	AddShader(shaderID, vertexCode, GL_VERTEX_SHADER); // add the shaders
	AddShader(shaderID, fragmentCode, GL_FRAGMENT_SHADER);

	GLint result{0};
	GLchar eLog[1024]{0};

	glLinkProgram(shaderID);	// link the program, creates the executables on the graphic card
	glGetProgramiv(shaderID, GL_LINK_STATUS, &result);	// check for errors
	if (!result) {
		glGetProgramInfoLog(shaderID, sizeof(eLog), NULL, eLog);
		std::cout << "Error linking program: " << eLog << std::endl;
		return;
	}

	glValidateProgram(shaderID);	// validate the program, make sure its valid for current opengl context
	glGetProgramiv(shaderID, GL_VALIDATE_STATUS, &result);
	if (!result) {
		glGetProgramInfoLog(shaderID, sizeof(eLog), NULL, eLog);
		std::cout << "Error validating program: " << eLog << std::endl;
		return;
	}

	uniformModel = glGetUniformLocation(shaderID, "model");
	uniformProjection = glGetUniformLocation(shaderID, "projection");
	uniformView = glGetUniformLocation(shaderID, "view");
	uniformDirectionalLight.uniformColour = glGetUniformLocation(shaderID, "directionalLight.base.colour");
	uniformDirectionalLight.uniformAmbientIntensity = glGetUniformLocation(shaderID, "directionalLight.base.ambientIntensity");
	uniformDirectionalLight.uniformDirection = glGetUniformLocation(shaderID, "directionalLight.direction");
	uniformDirectionalLight.uniformDiffuseIntensity = glGetUniformLocation(shaderID, "directionalLight.base.diffuseIntensity");
	uniformSpecularIntensity = glGetUniformLocation(shaderID, "material.specularIntensity");
	uniformShininess = glGetUniformLocation(shaderID, "material.shininess");
	uniformEyePosition = glGetUniformLocation(shaderID, "eyePosition");

	uniformPointLightCount = glGetUniformLocation(shaderID, "pointLightCount");

	for (size_t i = 0; i < MAX_POINT_LIGHTS; ++i) {

		std::ostringstream out;

		out << "pointLights[" << i << "].base.colour";
		uniformPointLight[i].uniformColour = glGetUniformLocation(shaderID, out.str().c_str());
		out.clear();
		out.str("");

		out << "pointLights[" << i << "].base.ambientIntensity";
		uniformPointLight[i].uniformAmbientIntensity = glGetUniformLocation(shaderID, out.str().c_str());
		out.clear();
		out.str("");

		out << "pointLights[" << i << "].base.diffuseIntensity";
		uniformPointLight[i].uniformDiffuseIntensity = glGetUniformLocation(shaderID, out.str().c_str());
		out.clear();
		out.str("");

		out << "pointLights[" << i << "].position";
		uniformPointLight[i].uniformPosition = glGetUniformLocation(shaderID, out.str().c_str());
		out.clear();
		out.str("");

		out << "pointLights[" << i << "].constant";
		uniformPointLight[i].uniformConstant = glGetUniformLocation(shaderID, out.str().c_str());
		out.clear();
		out.str("");

		out << "pointLights[" << i << "].linear";
		uniformPointLight[i].uniformLinear = glGetUniformLocation(shaderID, out.str().c_str());
		out.clear();
		out.str("");

		out << "pointLights[" << i << "].exponent";
		uniformPointLight[i].uniformExponent = glGetUniformLocation(shaderID, out.str().c_str());
		out.clear();
		out.str("");
	}

	uniformSpotLightCount = glGetUniformLocation(shaderID, "spotLightCount");

	for (size_t i = 0; i < MAX_SPOT_LIGHTS; ++i) {

		std::ostringstream out;

		out << "spotLights[" << i << "].base.base.colour";
		uniformSpotLight[i].uniformColour = glGetUniformLocation(shaderID, out.str().c_str());
		out.clear();
		out.str("");

		out << "spotLights[" << i << "].base.base.ambientIntensity";
		uniformSpotLight[i].uniformAmbientIntensity = glGetUniformLocation(shaderID, out.str().c_str());
		out.clear();
		out.str("");

		out << "spotLights[" << i << "].base.base.diffuseIntensity";
		uniformSpotLight[i].uniformDiffuseIntensity = glGetUniformLocation(shaderID, out.str().c_str());
		out.clear();
		out.str("");

		out << "spotLights[" << i << "].base.position";
		uniformSpotLight[i].uniformPosition = glGetUniformLocation(shaderID, out.str().c_str());
		out.clear();
		out.str("");

		out << "spotLights[" << i << "].base.constant";
		uniformSpotLight[i].uniformConstant = glGetUniformLocation(shaderID, out.str().c_str());
		out.clear();
		out.str("");

		out << "spotLights[" << i << "].base.linear";
		uniformSpotLight[i].uniformLinear = glGetUniformLocation(shaderID, out.str().c_str());
		out.clear();
		out.str("");

		out << "spotLights[" << i << "].base.exponent";
		uniformSpotLight[i].uniformExponent = glGetUniformLocation(shaderID, out.str().c_str());
		out.clear();
		out.str("");

		out << "spotLights[" << i << "].direction";
		uniformSpotLight[i].uniformDirection = glGetUniformLocation(shaderID, out.str().c_str());
		out.clear();
		out.str("");

		out << "spotLights[" << i << "].edge";
		uniformSpotLight[i].uniformEdge= glGetUniformLocation(shaderID, out.str().c_str());
		out.clear();
		out.str("");
	}
}

GLuint Shader::GetProjectionLocation() {
	return uniformProjection;
}

GLuint Shader::GetModelLocation() {
	return uniformModel;
}

GLuint Shader::GetViewLocation() {
	return uniformView;
}

GLuint Shader::GetAmbientIntensityLocation() {
	return uniformDirectionalLight.uniformAmbientIntensity;
}
GLuint Shader::GetAmbientColourLocation() {
	return uniformDirectionalLight.uniformColour;
}

GLuint Shader::GetDiffuseIntensityLocation() {
	return uniformDirectionalLight.uniformDiffuseIntensity;
}
GLuint Shader::GetDirectionLocation() {
	return uniformDirectionalLight.uniformDirection;
}

GLuint Shader::GetSpecularIntensityLocation() {
	return uniformSpecularIntensity;
}

GLuint Shader::GetShininessLocation() {
	return uniformShininess;
}

GLuint Shader::GetEyePositionLocation() {
	return uniformEyePosition;
}

void Shader::SetDirectionalLight(DirectionalLight *dLight) {

	dLight->UseLight(uniformDirectionalLight.uniformAmbientIntensity,
					uniformDirectionalLight.uniformColour,
					uniformDirectionalLight.uniformDiffuseIntensity,
					uniformDirectionalLight.uniformDirection);
}

void Shader::SetPointLights(PointLight *pLight, unsigned int lightCount) {

	if (lightCount > MAX_POINT_LIGHTS) lightCount = MAX_POINT_LIGHTS;

	glUniform1i(uniformPointLightCount, lightCount);

	for (size_t i = 0; i < lightCount; ++i) {

		pLight[i].UseLight(uniformPointLight[i].uniformAmbientIntensity,
							uniformPointLight[i].uniformColour,
							uniformPointLight[i].uniformDiffuseIntensity,
							uniformPointLight[i].uniformPosition,
							uniformPointLight[i].uniformConstant,
							uniformPointLight[i].uniformLinear,
							uniformPointLight[i].uniformExponent);
	}
}

void Shader::SetSpotLights(SpotLight *sLight, unsigned int lightCount) {

	if (lightCount > MAX_SPOT_LIGHTS) lightCount = MAX_SPOT_LIGHTS;

	glUniform1i(uniformSpotLightCount, lightCount);

	for (size_t i = 0; i < lightCount; ++i) {

		sLight[i].UseLight(uniformSpotLight[i].uniformAmbientIntensity,
							uniformSpotLight[i].uniformColour,
							uniformSpotLight[i].uniformDiffuseIntensity,
							uniformSpotLight[i].uniformPosition,
							uniformSpotLight[i].uniformDirection,
							uniformSpotLight[i].uniformConstant,
							uniformSpotLight[i].uniformLinear,
							uniformSpotLight[i].uniformExponent,
							uniformSpotLight[i].uniformEdge
							);
	}
}

void Shader::UseShader() {

	glUseProgram(shaderID);
}

void Shader::ClearShader() {

	if (shaderID != 0) {
		glDeleteProgram(shaderID);
		shaderID = 0;
	}

	uniformModel = 0;
	uniformProjection = 0;
}

void Shader::AddShader(GLuint theProgram, const char* shaderCode, GLenum shaderType) {

	GLuint theShader = glCreateShader(shaderType);	// create the shader

	const GLchar* theCode[1];
	theCode[0] = shaderCode;

	GLint codeLength[1];
	codeLength[0] = strlen(shaderCode);

	glShaderSource(theShader, 1, theCode, codeLength);	// put code into the shader
	glCompileShader(theShader);	// compile the shader

	GLint result{0};
	GLchar eLog[1024]{0};

	glGetShaderiv(theShader, GL_COMPILE_STATUS, &result);
	if (!result) {
		glGetShaderInfoLog(theShader, sizeof(eLog), NULL, eLog);
		std::cout << "Error compiling the " <<  shaderType << " " << eLog << std::endl;
		return;
	}

	glAttachShader(theProgram, theShader);	// add the shader to the program we defined
}

Shader::~Shader() {
	ClearShader();
}

